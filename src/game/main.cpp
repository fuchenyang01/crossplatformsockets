
#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

#if _WIN32
#include <Windows.h>
#endif

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );
	SocketUtil::StaticInit();

	SocketAddressPtr toAddress = SocketAddressFactory::CreateIPv4FromString("127.0.0.1:7");


	UDPSocketPtr Socket = SocketUtil::CreateUDPSocket(SocketAddressFamily::INET);
	Socket->SetNonBlockingMode(true);

	string sendStr("Hello World");

	Socket->SendTo(static_cast<const void*>(sendStr.c_str()), sendStr.length() + 1, *toAddress);

	SocketAddressPtr fromAddress(new SocketAddress);

	Socket->Bind(*fromAddress);

	const int BUFFER_MAX = 32;
	char buff[BUFFER_MAX];

	Socket->ReceiveFrom(static_cast<void*>(buff), BUFFER_MAX, *fromAddress);


	SocketUtil::CleanUp;


}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
